# DNS Repo

This repository contains all the necessary information related to the project's
DNS Records.

The repository should not be publicly visible and is accessible only to
maintainers and specific teams. They can view, send, and approve merge requests
(MRs) to update the DNS records.

Highlighted features of this repository include:

- **GitLab CI/CD Enabled** \
  All workflows related to the DNS repository are managed within GitLab. Users
  can send merge requests to propose changes, and maintainers can review these
  changes through the CI/CD log. Once a merge request is merged, the proposed
  changes are automatically applied.

- **Powered by [octoDNS][octodns]** \
  We rely on octoDNS to manage all our DNS records. It provides a robust and
  efficient solution for DNS as a Code, enabling streamlined management across
  multiple providers.

## Table of Contents

[[_TOC_]]

## Modifying DNS Records

We utilize [octoDNS][octodns] as our DNS as Code (DaaC) tool. OctoDNS offers a
comprehensive set of features and conventions for managing DNS records in a
code-driven manner. For detailed information about octoDNS, including
conventions, configurations, and more, please refer to their official repository
page. The repository provides a wealth of resources to help you get started and
make the most out of octoDNS for managing DNS records.

## Using this Template

To get started with this template, follow the steps below:

1. Fork this repo.

2. Clone this repo to your local workstation and start the [Development
   Environment](#starting-development-environment).

3. Configure the domain you want to manage in the `config/production.yaml` file.

4. If you already have an existing domain, you may want to dump the zone instead
   of manually copying all the records. Use the following command to achieve
   this:
   ```sh
   octodns-dump --config-file=config/production.yaml --output-dir=tmp/ example.com. cloudflare
   ```
    For more details, refer to the [Bootstrapping config
    files](https://github.com/octodns/octodns/#bootstrapping-config-files) guide
    in the octoDNS repository.

    Once the dump is complete, move and organize the generated configuration
    files into the `dns/` folder.

5. Commit and push the changes to your repo.

6. [Enable the CI/CD
   Feature](https://docs.gitlab.com/ee/ci/enable_or_disable_ci.html#enable-cicd-in-a-project)
   in your Repository.

7. Navigate to the Environment page and create an environment named
   `Production`.

8. Visit the CI/CD Settings page and provide the necessary tokens or API keys
   for your providers. For this repository, you need to add the following
   variables:
   - `CLOUDFLARE_TOKEN`, **Default** Environment: \
     Create a new Cloudflare API Key with the permissions Zone:Read and
     DNS:Read.
   - `CLOUDFLARE_TOKEN`, **Production** Environment: \
     Create a new Cloudflare API Key with the permissions Zone:Read, DNS:Read,
     and **DNS:Write**.

   Having two separate API keys ensures that merge request senders cannot modify
   the CI/CD script to run the synchronization in write mode.

9. Personalize this README file to provide specific details about your project,
   configurations, and usage instructions.

## Starting Development Environment

To set up the development environment, follow the steps below:

1. Ensure you have the following prerequisites installed:
   - Python 3
   - [venv](https://docs.python.org/3/library/venv.html)
   - [direnv](https://direnv.net/)

2. After cloning this repository, install the necessary dependencies by running
   the following commands:

   1. Install the virtual environment (venv):
       ```sh
       python -m venv env
       ```

   2. Copy the `.env.sample` file and rename it to `.env`. Fill in all the
      required tokens (if any). For example, if this project uses Cloudflare,
      generate a token and provide it in the `.env` file.

   3. If you haven't done so already, allow `direnv` to load the `.envrc` file
      by running the following command:
      ```sh
      direnv allow
      ```

   4. Reload the environment by running this command (run this if you're
      changing any `.env` or `.envrc` files):
      ```sh
      direnv reload
      ```
      Alternatively, you can close and reopen the terminal again.

3. Install the remaining dependencies:
    ```sh
    pip install -r requirements.txt
    ```

    For more detailed instructions, refer to the [Getting
    started](https://github.com/octodns/octodns/#getting-started) guide in
    octoDNS's repo.


## License
This template is licensed under [MIT](./LICENSE). Feel free to use this
repository.


[octodns]: https://github.com/octodns/octodns/
